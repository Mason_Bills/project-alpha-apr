from projects.views import projects
from django.urls import path

urlpatterns = [
    path("", projects, name="list_projects"),
]
